angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})
.factory('scannerFactory', function(){
  console.log('scanner factory');

  var scan = function(){
    ionic.Platform.ready(function(){
        scanner.startScanning(function(){}, function(result){console.log(result)});
    })
  }
  var scanPartial = function(){
    ionic.Platform.ready(function(){
        scanner.startScanning(50,50,200,100);
    })
  }
  var scanImage = function(){
    function init(Scanner, codes){
      Scanner.MWBregisterCode(codes.MWB_CODE_MASK_QR, "lperez@hexacta.com", "1EE9A87A6D0E47FF8FB84B94B36F87974FC951D2D567101E2172F3B14E36BBDE");
    }
    function cbk(result){
      console.log('result: ', result);
    }
    ionic.Platform.ready(function(){
        scanner.scanImage(init, cbk,"img/barcode.png");
    })
  }
  return {
    scan: scan,
    scanPartial: scanPartial,
    scanImage: scanImage
  };
}).factory('lockFactory', function($ionicBackdrop, $rootScope){

  var attemptCount = 3;
  var backdrop = false;
  var prompt = function(msg){

    $ionicBackdrop.retain();
    backdrop = true;

    var callback = function(result){
      console.log(result);
      if(result.buttonIndex == 1){

        if(result.input1 == 123456){
          $ionicBackdrop.release();
            console.log('Pin verificado');
        }else{
          if(attemptCount == 0){
            alert('Aplicacion bloqueada');
          }else{
            prompt('Pin incorrecto, tiene '+attemptCount+' intentos restantes');
            attemptCount -= 1;
          }
        }
      }else{
          window.plugins.pinDialog.prompt(msg, callback, 'Acceder', ['OK']);
      }
    }

    window.plugins.pinDialog.prompt(msg, callback, 'Acceder', ['OK']);
  }
  return {
    prompt: prompt
  }

})
