var notificationService = function($rootScope, $http, $queue){
	
	var queue = $queue.queue(proccessQueue, options);

	var processNotification = function(data){
		$http.post(url,data).then(function(response){
			$rootScope.$broadcast('notification-sent', response)
			deleteNotification(data.id)//elimina notificacion de local storage
		}, function(err){
			data.attempts += 1;
			if(data.attemps > limit){
				$rootScope.$broadcast('notification-error', response)	
			}else{
				saveNotification(data);	
			}
			
		})

	}
	var processQueue = function(data){
		var notifications = getnotifications(); //levanta las notificaciones de localstorage
		queue.addEach(notifications);
		queue.start();
	}
	var pauseQueue = function(){
		queue.pause();
	}
	var addItem = function(data){
		saveNotification(data);// guarda notificacion el localstorage
		data.id = uid();
		data.attempts = 0;
	}
	return{
		addItem: addItem,
		processQueue: processQueue,
		pauseQueue: pauseQueue
	}

}
notificationService.$inject = ['$rootScope', '$http', '$queue']
angular.module('app').factory('notificationService', notificationService);